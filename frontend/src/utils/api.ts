export async function fetchCurrencyRates(endpoint: string) {
  const url = `http://localhost:3000${endpoint}`
  try {
    const response = await fetch(url)
    const data = await response.json()
    console.log('response', data)
    return Object.entries(data.rates).map(([key, value]) => ({
      fromCurrency: 'CUPCAKE',
      toCurrency: key,
      value,
    }))
  } catch (error) {
    console.error('Error fetching currency rates:', error)
    throw error
  }
}
