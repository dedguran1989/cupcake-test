import React from 'react'
import ReactDOM from 'react-dom'
import CurrencyTable from './components/CurrencyTable'

const App = () => <CurrencyTable />

ReactDOM.render(<App />, document.getElementById('root'))
