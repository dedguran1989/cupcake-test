import React, { useCallback, useEffect, useState } from 'react'
import { fetchCurrencyRates } from '../utils/api'
import { calculateAdditionalPairs } from '../helpers'

import './CurrencyTable.css'
import { CurrencyRate } from '../types'

const CurrencyTable = () => {
  const [rates, setRates] = useState<{ [key: string]: number[] }>({})

  useEffect(() => {
    const endpoints = [
      '/api/v1/first/poll',
      '/api/v1/second/poll',
      '/api/v1/third/poll',
    ]
    const fetchRates = useCallback(async () => {
      try {
        const updates = await Promise.all(
          endpoints.map((endpoint) => fetchCurrencyRates(endpoint)),
        )
        // Объединение данных из всех источников в один объект
        const combinedRates: { [key: string]: number[] } = updates.reduce(
          (acc: { [key: string]: number[] }, current, index) => {
            current.forEach((rate) => {
              const key = `${rate.fromCurrency}/${rate.toCurrency}`
              if (!acc[key]) {
                acc[key] = Array(3).fill(null)
              }
              acc[key][index] = rate.value as number
            })
            return acc
          },
          {},
        )
        // Вычисление новых валютных пар
        const allRates = calculateAdditionalPairs(combinedRates)
        setRates(allRates)
      } catch (error) {
        console.error('Failed to fetch currency rates:', error)
      }
    }, [])

    fetchRates()
    const intervalId = setInterval(fetchRates, 10000) // Fetch every 10 seconds
    return () => clearInterval(intervalId)
  }, [])

  return (
    <table className="currency-table">
      <thead>
        <tr>
          <th>Pair name</th>
          <th>First</th>
          <th>Second</th>
          <th>Third</th>
        </tr>
      </thead>
      <tbody>
        {Object.entries(rates).map(([key, values]) => (
          <tr key={key}>
            <td>{key}</td>
            {values.map((value, idx) => (
              <td
                className={value === Math.min(...values) ? 'highlight' : ''}
                key={idx}
              >
                {value !== null ? value.toFixed(3) : 'N/A'}
              </td>
            ))}
          </tr>
        ))}
      </tbody>
    </table>
  )
}

export default CurrencyTable
