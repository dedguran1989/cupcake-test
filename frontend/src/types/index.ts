export interface CurrencyRate {
  fromCurrency: string
  toCurrency: string
  value: number
  source: string
}

export interface CurrencyEndpoint {
  url: string
}

export interface Rates {
  [key: string]: number[]
}
