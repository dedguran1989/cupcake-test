import { Rates } from '../types'

export const calculateAdditionalPairs = (rates: Rates): Rates => {
  const newRates = { ...rates }

  const cupcakeRubKey = 'CUPCAKE/RUB'
  const cupcakeUsdKey = 'CUPCAKE/USD'
  const cupcakeEurKey = 'CUPCAKE/EUR'

  newRates['RUB/USD'] = rates[cupcakeRubKey].map(
    (rub, idx) => rub / rates[cupcakeUsdKey][idx],
  )
  newRates['RUB/EUR'] = rates[cupcakeRubKey].map(
    (rub, idx) => rub / rates[cupcakeEurKey][idx],
  )
  newRates['EUR/USD'] = rates[cupcakeEurKey].map(
    (eur, idx) => eur / rates[cupcakeUsdKey][idx],
  )

  return newRates
}
